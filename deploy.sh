#!/bin/bash
set -eo pipefail

if [[ -z $STACK_NAME ]]; then
  >&2 echo "Debe especificar el nombre del stack"
  exit 1
fi

list_services() {
  docker service
}

inspect_service() {
  docker service inspect $service
}

update_service() {
  local service=$1
  local image=$2:$3
  local revert=$4

  if [[ -z $service ]]; then
    >&2 echo "Falto especificar el nombre del servicio"
    return 1
  fi

  local version=$(inspect_service $service | jsonval Index)

  if [[ -z $version ]]; then
    >&2 echo "Falto especificar la nueva version del servicio"
    return 1
  fi

  if [[ $revert = 'previous' ]]; then
    docker service rollback $service
  else
    docker service update --image $image:$version $service
  fi
}

update_service "${STACK_NAME}_${1}" $2 $3
